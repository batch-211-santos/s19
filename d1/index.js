// console.log("Hello World");

//What are conditional statements?

//Conditional Statements allow us to control the flow of our program
//It allows us to run a statement/instruction if a condition is met or run another seperate instruction if otherwise

//if, else if, and else statement

let numA = -1;

//if statement
	//executes a statement if a specified condition is true

	if(numA<0){
		console.log("Yes! It is less than zero")
	};

	/*
		Syntax:

		if(condition){
			statement
		}

	*/
	//the result of the expression added in the if's condition must result to true, else, the statement inside if() will not run
	console.log(numA<0);//results to true -- the if statement will run

	numA = 0;

	if(numA<0){
		console.log("Hello")
	};

	console.log(numA<0); //false

	let city = "New York"

	if(city === "New York"){
		console.log("Welcome to New York City!")
	};

	//else if clause

	/*
		-executes a statement if previous conditions are false and if the specified condition is true
		-The "else if" clause is optional and can be added to capture additonal conditions to change the flow of a program

	*/

	let numH = 1;

	if(numA<0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	}

	//we were able to run the else if() statement after we evaluated that the if condtion was failed

	//if the if() condition was passed and run, we will no longer evaluate the else if() and end the process there

	numA = 1;

	if(numA>0){
		console.log("Hello");
	} else if (numH>0){
		console.log("World");
	}

	//else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stops there

	city = "Tokyo"

	if(city === "New York"){
		console.log("Welcome to New York City!")
	} else if(city === "Tokyo"){
		console.log("Welcome to Tokyo, Japan!")
	}

	//since we failed the condition for the first if(), we went to the else if() and checked and instead passed that condition

	//else statement

	/*
		-executes a statement if all other conditions are false
		-the "else" statement is optional and can be added to capture any other result to change the flow of a program

	*/

	if(numA<0){
		console.log("Hello");
	} else if (numH === 0) {
		console.log("World");
	} else {
		console.log("Again")
	}

	/*
		Since both the preceeding if and else if conditions are not met/failed, the else statenent was run instead

		Else statements should only be added if there is a preceeding if condition.
		Else statements by itself will not work, howver, if statements will work evn if there is no else statement

	*/

	// else{
	// 	console.log("Will not run without an if")
	// }

	// else if (numH === 0){
	// 	console.log('World')
	// } else {
	// 	console.log('Again')
	// }

	//same goes for an else if, there should be a preceding if() first

	//if, else if, and else statements with functions

	/*
		-most of the times, we would like to use if, else if, and else statements with functions to control the flow of our apppliation

	*/

	let message = "No message.";
	console.log(message);

	function determineTyphoonIntensity(windSpeed){

		if (windSpeed < 30){
			return 'Not a typhoon yet.';
		}else if (windSpeed <= 61) {
			return 'Tropical depression detected.';
		}else if (windSpeed >= 62 && windSpeed <= 88){
			return 'Tropical Storm detected.'
		}else if(windSpeed >= 89 || windSpeed <= 117){
			return 'Severe tropical storm detected.'
		}else{
			return 'Typhoon Detected';
		}

	}
	//returns the string to the variable "message" that invoked it
	message = determineTyphoonIntensity(110);
	console.log(message);

	if (message == "Severe tropical storm detected.") {
		console.warn(message);
	}
	//console.warn() is a good way to print warnings in our console that could help us developers act on certain output with out code

	// Mini Activity #1

	function oddOrEvenChecker(num){
		if(num % 2 === 0){
			alert(num + " is even!")
		}else{
			alert(num + " is odd!")
		}
	}

	// oddOrEvenChecker(54);

	//Mini Activity #2

	function ageChecker(num){
		if(num <17){
			alert(num + " is underaged");
			return(false);
		}else{
			alert(num + " is allowed to drink!")
			return true
		}
	}

	// let isAllowedToDrink = ageChecker(19);
	// console.log(isAllowedToDrink);


	//Truthy and Falsy

	/*
		- In JavaScript a "truthy" value is a value that is considered true when encountered in Boolean context
		-Values in are considered true unless defined otherwise
		-Falsy Values/exceptions for truth:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6.undefined
			7. NaN

	*/

	//Truthy Examples
	/*
		-If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
		-Expressions are any unit of code that can be evaluated to a value
	*/

	if (true){
		console.log('Truthy');
	}

	if (1){
		console.log('Truthy');
	}

	if ([]){
		console.log('Truthy');
	}


	//Falsy Examples

	if (false){
		console.log('Falsy');
	}

	if (0){
		console.log('Falsy');
	}

	if (undefined){
		console.log('Falsy');
	}

	//Conditional (Ternary) Operator

	/*
		-The Conditional (Ternary) Operator takes in three operands:
			1. condition
			2. expression to execute if the condition is truthy
			3. expression to execute if the condition is falsy

		-can be used as an alternative to an "if else" statement
		-ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable

		-Syntax

			(expression) ? ifTrue: ifFalse;

	*/


	//Single statement execution

	let ternaryResult = (1<18) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult)
	console.log(ternaryResult);

	//Multiple statement execution
	/* a function may be defined then used in a ternary operator
	*/

	let name;

	function isOfLegalAge(){
		name = "John";
		return 'You are of the legal age limit';
	}

	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit"
	}



	let age = parseInt(prompt("What is your age?"));
	console.log(age);
	let legalAge = (age>18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

	//Switch Statement
	/*
		The switch statement evaluates an expression and matches the expression's value to a case clause

		-.toLowerCase Function will change the input received from the prompt into all lowercase letters ensuring a match with the switch case
		-The "expression" is the information used to match the "value" provided in the switch cases
		-Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
		-break statement is used to terminate the current loop once a match has been found

	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day) {
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is blue");
			break;
		case 'saturday':
			console.log("The color of the day is indigo");
			break;
		case 'sunday':
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please input a valid day");
			break;
	}
	

	// Mini Activity #3

	function determineBear(bearNumber){
		let bear;
		switch(bearNumber){
			case 1:
				alert("Hi, I'm Amy");
				break;
			case 2:
				alert("Hi, I'm Lulu!");
				break;
			case 3:
				alert("Hi, I'm Morgan!");
				break;
			default:
				bear = bearNumber + 'is out of bounds.';
				break;
		}
		return bear;
	}
	determineBear(2);

	//Try-Catch-Finally Statement
/*
    - "try catch" statements are commonly used for error handling
    - There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
    - These errors are a result of an attempt of the programming language to help developers in creating efficient code
    - They are used to specify a response whenever an exception/error is received
    - It is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement
    - In most programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
    - The "finally" block is used to specify a response/action that is used to handle/resolve errors
*/

	function showIntensityAlert(windSpeed) {
		try {
			alert(determineTyphoonIntensity(windSpeed))
		   // error/err are commonly used variable names used by developers for storing errors
		}catch (error){
			 // The "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is
			console.log(typeof error);
			// Catch errors within 'try' statement
	        // In this case the error is an unknown function 'alerat' which does not exist in Javascript
	        // The "alert" function is used similarly to a prompt to alert the user
	        // "error.message" is used to access the information relating to an error object
			console.warn(error.message);
		}finally {
			//continue execution of code regardless of success and failure of code execution in the 'try' block to hdanle/resolve errors
			alert('Intensity updates will show new alert.')
		}
	}

	showIntensityAlert(56);